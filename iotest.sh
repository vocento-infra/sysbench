#! /bin/bash

date

for block_s in 1 4 16 32 64 128 256 512
do
	sysbench --threads=${threads} --file-block-size=${block_s}K \
	fileio --file-test-mode=${ftm} --file-io-mode=sync --file-extra-flags=direct \
	--file-total-size=2G prepare &>/dev/null

	for ftm in seqrd seqwr rndrd rndwr
	do
		for threads in 1 4 16 32 
		do
			echo "----------------"
			echo "Modo: $ftm"
			echo "Threads: $threads"
			echo "Bloque: ${block_s}k"
			echo "----------------"
	
			echo 3 > /proc/sys/vm/drop_caches

			sysbench --threads=${threads} --file-block-size=${block_s}K \
			fileio --file-test-mode=${ftm} --file-io-mode=sync --file-extra-flags=direct \
			--file-total-size=2G --time=120 --events=1000000 run | grep -A 7 'File operations'

		done
	done

	sysbench --threads=${threads} --file-block-size=${block_s}K \
	fileio --file-test-mode=${ftm} --file-io-mode=sync --file-extra-flags=direct \
	--file-total-size=2G cleanup &>/dev/null

done

date